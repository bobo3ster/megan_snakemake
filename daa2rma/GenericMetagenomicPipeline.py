import glob

#Set DIAMOND and DAA2RMA to the right location
DIAMOND = 'diamond '
DAA2RMA = 'daa2rma '

# databases and mapping files
FILESDIR = 'files/'
NRFILE = FILESDIR + 'nr.gz'
NRDIAMONDDATABASENAME = FILESDIR + 'nr'
NRDIAMONDDATABASE = FILESDIR + 'nr.dmnd'
KEGGFILE = FILESDIR + 'gi2kegg.bin'
GI2TAXIDFILE = FILESDIR + 'gi2taxid.bin'



#Data folders. 00fastq has to exist with all fastq.gz files inside. All others will be created by the workflow manager

FASTQINFOLDER = '00fastq/'
DAAFOLDER = '10daa/'
RMAFOLDER = '20rma/'

#Get all fastq files
FASTQFILES = [file.replace('.fastq.gz','').replace(FASTQINFOLDER, '')for file in glob.glob(FASTQINFOLDER + '*.fastq.gz')]



#Snakemake rules
rule all:
  input:
    expand(RMAFOLDER + '{sample}.rma', sample=FASTQFILES)


rule makedb:
    input:
        NRFILE
    output:
        touch(FILESDIR + 'nr.dbBuild')
    shell:
        DIAMOND + ' makedb --in {input} --db {NRDIAMONDDATABASENAME}'

rule align:
    input:
        fastq = FASTQINFOLDER + '{sample}.fastq.gz',
        reference = FILESDIR + 'nr.dbBuild'
    output:
        DAAFOLDER + '{sample}.daa'
    threads:
        24
    shell:
        DIAMOND + ' blastx --query {input} --db {NRDIAMONDDATABASENAME} --daa {output} -p {threads}'

rule daa2rma:
    input:
        daa = DAAFOLDER + '{sample}.daa',
        kegg = KEGGFILE,
        gi2taxid = GI2TAXIDFILE
    output:
        RMAFOLDER + '{sample}.rma'
    shell:
        DAA2RMA + ' -i {input.daa} -o {output} -g2t {input.gi2taxid} -g2kegg {input.kegg} -fun KEGG'
